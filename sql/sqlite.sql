create table label (
    id integer primary key,
    brewery text,
    beer text,
    year text,
    notes text,
    created datetime default current_timestamp,
    updated datetime default current_timestamp
);

create table labelimage (
    id integer primary key,
    basename text,
    size unsigned big int,
    guid varchar(36),
    created datetime default current_timestamp
);

create table labeltoimage (
    labelid integer,
    labelimageid integer,
    created datetime default current_timestamp,
    deleted datetime default NULL,
    foreign key(labelid) references label(id),
    foreign key(labelimageid) references labelimage(id),
    primary key(labelid, labelimageid)
);

create table imagetothumb (
    imageid integer,
    thumbid integer,
    created datetime default current_timestamp,
    foreign key(imageid) references labelimage(id),
    foreign key(thumbid) references labelimage(id),
    primary key(imageid, thumbid)
);
