package LabelHistory;
use Dancer ':syntax';

use LabelHistory::FileManager;
use LabelHistory::Label;
use LabelHistory::ManageLabel;
use LabelHistory::Users;

use Data::Dumper;

our $VERSION = '0.1';

my $label_manager = LabelHistory::ManageLabel->new();
my $file_manager  = LabelHistory::FileManager->new();
my $users = LabelHistory::Users->new();

# A regex to match the pages that can be used without authentication
my $safe_paths = qr[^/(?:$|login|view|search|label|newuser)];

hook 'before' => sub {
    var fields => $LabelHistory::Label::fields;
    var field_order => $LabelHistory::Label::field_order;
    if ( !session('userid') && request->path_info !~ $safe_paths ) {
        var requested_path => request->path_info;
        request->path_info('/login');
    }
};

get '/' => sub {
    template 'index', { page => 'home' };
};

get '/add' => sub {
    template 'addedit', { page => 'add' };
};

post '/add' => sub {
    my $id;
    if (param('id')) {
        $id = $label_manager->update(params);
    } else {
        $id = $label_manager->add(params);
    }
    redirect '/view/' . $id;
};

get '/edit/:id' => sub {
    my $label = $label_manager->get( param('id') );
    unless ($label) {

        # TODO make a template
        status 'not_found';
        return ();
    }
    template 'addedit', { label => $label, page => 'edit' };
};

get '/view/:id' => sub {
    my $label = $label_manager->get( param('id') );
    unless ($label) {

        # TODO make a template
        status 'not_found';
        return ();
    }
    template 'view', { label => $label, page => 'view' };
};

post '/upload' => sub {
    my $results = $file_manager->handle_upload( request->upload('labels') );
    content_type 'application/json';
    return to_json { files => $results };
};

get qr'^/label/([^/]*)(?:/.*)' => sub {
    my @vals = splat;
    my ( $file, $filetype, $filename ) =
      $file_manager->get_file_details_for_guid( $vals[0] );
    unless ($file) {

        # TODO template
        status 'not_found';
        return ();
    }
    send_file(
        $file,
        content_type => $filetype,
        system_path  => 1,
    );
};

get '/removeupload/:method/:id' => sub {
    my $method = param('method'); # 'html' or 'json'
    my $id = param('id');
    if ($method !~ /^(?:html|json)$/) {
        status 'not_found';
        return ();
    }
    # TODO security by ownership
    my $result = $file_manager->remove_file_by_id($id);
    if ($method eq 'html') {
        redirect '/edit/' . $id;
    } elsif ($method eq 'json') {
        content_type 'application/json';
        return to_json { success => 1 };
    }
};

get '/typeahead/:class/:query' => sub {
    content_type 'application/json';
    return to_json [{value => 'this is a long sentence to see how trimming works and such'},{value => 'test2'},{value => 'test3'},];
};

get '/login' => sub {
    template 'login',
      {
        page           => 'login',
        requested_path => ( var 'requested_path' ) || ( param 'requested_path' )
      };
};

post '/login' => sub {
    my $user = $users->login( param 'user', param 'password' );
    if ($user) {
        session userid   => $user->userid;
        session name     => $user->name;
        session username => $user->username;
        redirect( param('requested_path') || '/' );
    }
    else {
        template 'login',
          {
            page           => 'login',
            requested_path => param 'requested_path',
          };
    }
};

get '/logout' => sub {
    session->destroy;
    redirect '/';
};

true;
