package LabelHistory::Users;

# Copyright (C) 2014 Robin Sheat
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 NAME

LabelHistory::Users - manage user details

=head1 FUNCTIONS

=cut

use base (Class::Accessor);

use autodie;
use Dancer;
use Dancer::Plugin::Database;
use Data::Dumper;
use LabelHistory::User;

=head2 new

    my $users = LabelHistory::Users->new();

Create a new instance of the users class.

=head2 login

    my $user = $users->login($username, $password);

Takes the user supplied username and password, validates it, and returns
their L<LabelHistory::Users> object if everything checks out.

=cut

sub login {
    my ($self, $username, $password) = @_;

    # TODO implement
    return LabelHistory::User->new(
        { userid => 1, username => 'username', name => 'John Doe' } );
}

true;
