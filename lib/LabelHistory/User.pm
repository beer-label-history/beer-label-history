package LabelHistory::User;

# Copyright (C) 2014 Robin Sheat
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 NAME

LabelHistory::User - contains user details

=head1 FUNCTIONS

=head2 userid

Return the numeric userid

=head2 username

Return the username

=head2 name

Return the user's name

=cut

use base (Class::Accessor);

__PACKAGE__->mk_ro_accessors(qw( userid username name ));

true;
