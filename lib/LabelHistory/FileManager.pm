package LabelHistory::FileManager;

# Copyright (C) 2014 Robin Sheat
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 NAME

LabelHistory::FileManger - handles file functions

=head1 FUNCTIONS

=cut

use base (Class::Accessor);

use autodie;
use Dancer;
use Dancer::Plugin::Database;
use Data::Dumper;
use Data::GUID;
use Image::Magick;
use LabelHistory::Image;

# This holds the MIME type of the acceptable filetypes.
my %filetypes = (
    jpg  => 'image/jpeg',
    jpeg => 'image/jpeg',
    gif  => 'image/gif',
    png  => 'image/png',
);

=head2 handle_upload

    $result = $manager->handle_upload($upload);

Takes an upload object of type L<Dancer::Request::Upload> and stores the file.
It returns something that can be sent to jQuery-FileUpload.

=cut

sub handle_upload {
    my ( $self, $upload ) = @_;

    my $file_id = $self->save_file($upload);
    my $thumb_id = $self->make_thumbnail($file_id);
    my $url     = $self->get_url($thumb_id);
    my %file    = (
        name => $upload->filename,
        url  => $url,
        id   => $file_id,

        #        deleteUrl  => '/deleteupload/id',
        #        deleteType => 'DELETE',
    );
    return [ \%file ];
}

=head2 save_file

    my $file_id = $manager->save_file($upload);

Takes an upload object of type L<Dancer::Request::Upload> and saves the file
in its final place. It returns an ID that can be used to refer to that file.

=cut

sub save_file {
    my ( $self, $upload ) = @_;

    my $name = $upload->basename;
    my $size = $upload->size;
    my $guid = Data::GUID->new->as_string;
    my $dbi  = database();
    my $id;
    $dbi->begin_work;
    eval {
        my $sth = $dbi->prepare(
            'INSERT INTO labelimage(basename, size, guid) VALUES (?, ?, ?)')
          or die $dbi->errstr;
        $sth->execute( $name, $size, $guid );
        $id = $dbi->last_insert_id( '', '', '', '' )
          or die "Can't get last ID";
        my $upload_dir = config->{upload_dir};
        $upload->copy_to( $upload_dir . '/' . $guid );
        unlink $upload->tempname;
    };
    if ($@) {
        $dbi->rollback;
        die $@;
    }
    else {
        $dbi->commit;
    }
    return $id;
}

=head2 make_thumbnail

    my $thumb_id = $manager->make_thumbnail($image_id);

Given an image id, create a thumbnail of it and associate it with the original
image. Returns the ID of the newly created image.

=cut

sub make_thumbnail {
    my ($self, $image_id) = @_;

    my ( $filename, undef, $basename ) =
      $self->get_file_details_for_id($image_id);
    my $guid = Data::GUID->new->as_string;
    my $upload_dir = config->{upload_dir};
    my $outfn = $upload_dir . '/' . $guid;
    my $thumb_basename = $basename;
    $thumb_basename =~ s/(\.[^.]*)$/.thumb$1/;
    my $img = Image::Magick->new;
    $img->Read($filename) and die "Unable to read image file $filename";
    $img->Resize(geometry => '150x150^');
    $img->Extent(geometry => '150x150', gravity => 'Center');
    $img->Write(magick => 'jpg', filename => $outfn) and die "Unable to write image file $outfn";
    my $thumb_id;
    my $dbi = database;
    eval {
        $dbi->begin_work;
        my $size = -s $outfn;
        my $sth = $dbi->prepare(
            'INSERT INTO labelimage(basename, size, guid) VALUES (?, ?, ?)')
            or die $dbi->errstr;
        $sth->execute( $thumb_basename, $size, $guid );
        $thumb_id = $dbi->last_insert_id( '', '', '', '' )
            or die "Can't get last ID";
        $sth = $dbi->prepare(
            'INSERT INTO imagetothumb(imageid, thumbid) VALUES (?, ?)'
        );
        $sth->execute($image_id, $thumb_id);
    };
    if ($@) {
        $dbi->rollback;
        die $@;
    } else {
        $dbi->commit;
    }
    return $thumb_id;
}

=head2 get_url {

    my $url = $manager->get_url($id);

Given an ID of an image, this gives you the URL part for it, of the form
C<guid/basename>.

Returns C<undef> if the image doesn't exist.

=cut

sub get_url {
    my ( $self, $id ) = @_;

    my $dbi = database();
    my $sth = $dbi->prepare('SELECT guid, basename FROM labelimage WHERE id=?');
    $sth->execute($id) or die $sth->errstr;
    my $row = $sth->fetchrow_arrayref;
    return undef unless $row;
    my $guid = $row->[0];
    my $name = $row->[1];
    return $guid . '/' . $name;
}

=head2 get_urls_for_label

    my $urls = $manager->get_urls_for_label($labelid, [$type]);

Given an ID of a label, this returns a hashref of the URL parts of all the
associated images. C<$type> can be 'FULL' or 'THUMB' to get the respective
image. The default is 'FULL'.

=cut

sub get_urls_for_label {
    my ( $self, $id, $type ) = @_;

    $type ||= 'FULL';
    my $dbi = database();
    my $sth = $dbi->prepare(
'SELECT id, guid, basename FROM labelimage JOIN labeltoimage ON (id=labelimageid) WHERE labelid=?'
    );
    $sth->execute($id) or die $sth->errstr;
    # Replace the query if we want thumbnails
    if ( $type eq 'THUMB' ) {
        my @ids = map { $_->[0] } @{ $sth->fetchall_arrayref };
        my @thumb_ids = $self->get_thumbnails(@ids);
        $sth = $dbi->prepare(
            'SELECT id, guid, basename FROM labelimage WHERE id IN ('
              . join(',', map { '?' } @thumb_ids). ')'
        );
        $sth->execute(@thumb_ids);
    }
    my @urls;
    while ( my $row = $sth->fetchrow_arrayref ) {
        unless ($row->[0]) {
            warn "No thumbnail found for label $id";
            next;
        }
        my $guid = $row->[1];
        my $name = $row->[2];
        push @urls, $guid . '/' . $name;
    }
    return \@urls;
}

=head2 get_images_for_label

    my $images = $manager->get_images_for_label($id);

Given a label ID, this returns a list of C<LabelHistory::Image> objects
containing the required image data.

=cut

sub get_images_for_label {
    my ($self, $id) = @_;

    my $dbi = database();
    my $sth = $dbi->prepare(
'SELECT id, guid, basename FROM labelimage JOIN labeltoimage ON (id=labelimageid) WHERE labelid=? AND labeltoimage.deleted IS NULL'
    );
    $sth->execute($id) or die $sth->errstr;
    my @images;
    while (my $row = $sth->fetchrow_arrayref) {
        my $id = $row->[0];
        my $guid = $row->[1];
        my $basename = $row->[2];
        my $url = $guid . '/' . $basename;
        my ($thumbnail) = $self->get_images($self->get_thumbnails($id));
        my $image = LabelHistory::Image->new(
            {
                id        => $id,
                guid      => $guid,
                basename  => $basename,
                url       => $url,
                thumbnail => $thumbnail
            }
        );
        push @images, $image;
    }
    return \@images;
}

=head2 get_images

    my @images = $manager->get_images(@ids);

Given a list of IDs, this creates L<LabelHistory::Image> objects for each one.
This won't attach thumbnails.

Any invalid ID will be C<undef>.

=cut

sub get_images {
    my ($self, @ids) = @_;

    my $dbi = database;
    my $sth = $dbi->prepare('SELECT id, guid, basename FROM labelimage WHERE id=?');
    my @imgs;
    foreach my $id (@ids) {
        $sth->execute($id);
        my $row = $sth->fetchrow_hashref;
        if (!$row) {
            push @imgs, undef;
        } else {
            push @imgs, LabelHistory::FileManager->new($row);
        }
    }
    return @imgs;
}

=head2 get_thumbnails

    my @thumb_ids = $manager->get_thumbnails(@image_ids);

For an array of image IDs, get the thumbnails for it. An entry will be C<undef>
if no thumbnail exists.

=cut

sub get_thumbnails {
    my ($self, @image_ids) = @_;

    my @res;
    my $dbi = database;
    my $sth = $dbi->prepare(
        'SELECT thumbid FROM imagetothumb WHERE imageid=?');
    foreach my $tid (@image_ids) {
        $sth->execute($tid);
        my $row = $sth->fetchrow_arrayref;
        if ($row) {
            push @res, $row->[0];
        } else {
            push @res, undef;
        }
    }
    return @res;
}

=head2 get_file_details_for_guid

    my ( $file, $filetype, $filename ) =
      $manager->get_file_details_for_guid($guid);

Returns details on a file given the GUID. C<$file> is the system path,
C<$filetype> is the MIME type, and C<$filename> is the filename to provide to
the client.

Returns C<undef> if the file doesn't exist.

=cut

sub get_file_details_for_guid {
    my ( $self, $guid, $id ) = @_;

    my $data =
      database->quick_select( 'labelimage',
        defined($id) ? { id => $id } : { guid => $guid } );
    return undef unless $data;
    my $basename   = $data->{basename};
    my $upload_dir = config->{upload_dir};

    my $file     = $upload_dir . '/' . $data->{guid};
    my $filetype = $self->guess_filetype($basename);
    return ( $file, $filetype, $basename );
}

=head2 get_file_details_for_id

    my ( $file, $filetype, $filename ) =
      $manager->get_file_details_for_id($id);

Returns details on a file given the ID. C<$file> is the system path,
C<$filetype> is the MIME type, and C<$filename> is the filename to provide to
the client.

Returns C<undef> if the file doesn't exist.

=cut

sub get_file_details_for_id {
    shift()->get_file_details_for_guid(undef, shift());
}

=head2 guess_filetype

    my $mimetype = $manager->guess_filetype($name);

Given a filename with a extension, this guesses the mimetype. If it can't
guess, it returns C<undef>, this probably means it's an illegal filetype.

=cut

sub guess_filetype {
    my ( $self, $name ) = @_;

    my ($ext) = $name =~ /\.(.*)$/;
    my $filetype = $filetypes{lc $ext};
    return $filetype;
}

=head2 remove_file_by_id

    my $result = $manager->remove_file_by_id($id);

Given the id of a file, this detaches it from any labels it's attached to. It
doesn't bother deleting it, because what's the point.

=cut

sub remove_file_by_id {
    my ($self, $id) = @_;

    my $dbi = database;
    $dbi->prepare('UPDATE labeltoimage SET deleted=current_timestamp WHERE labelimageid=?')->execute($id);
    return 1;
}

true;
