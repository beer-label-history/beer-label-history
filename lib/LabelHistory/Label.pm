package LabelHistory::Label;

use base (Class::Accessor);

# This specifies the fields in a label, it is pushed through to the template for it to
# render the entry fields. This is a bit too deep for UI stuff, but it's better
# to have it centralised.
our $fields = {
    beer => {
        label       => 'Beer',
        type        => 'text',
        exampletext => 'Pernicious Weed, Beer Geek Brunch Weasel, ...',
        helptext    => 'The name of the beer.',
        width       => 8,
    },
    brewery => {
        label          => 'Brewery',
        type           => 'text',
        exampletext    => 'Garage Project, Mikkeller, ...',
        helptext       => 'The brewery that made this beer.',
        width          => 8,
        typeaheadclass => 'brewery',
    },
    year => {
        label       => 'Year',
        type        => 'text',
        exampletext => '1999',
        helptext    => 'When the label was collected.',
        width       => 3,
    },
    notes => {
        label    => 'Notes',
        type     => 'textarea',
        width    => 8,
        helptext => 'Any notes about the label.',
    },
};
our $field_order = [qw( brewery beer year notes )];

LabelHistory::Label->mk_ro_accessors( @$field_order,
    qw( created, updated, labelimages labelthumbs, timezone ) );

# date things will be converted to a local timezone, then made human readable
sub created {
    my ($self)   = @_;
    my $date_str = $self->SUPER::created;
    my $dt       = DateTime::Format::SQLite($date_str);
    $dt->set_time_zone($timezone);
    return $dt->as_string();
}

sub updated {
    my ($self)   = @_;
    my $date_str = $self->SUPER::updated;
    my $dt       = DateTime::Format::SQLite($date_str);
    $dt->set_time_zone($timezone);
    return $dt->as_string();
}

true;
