package LabelHistory::ManageLabel;

use base(Class::Accessor);

use autodie;
use Dancer;
use Dancer::Plugin::Database;
use LabelHistory::FileManager;
use LabelHistory::Label;

use Data::Dumper;

sub add {
    my ( $self, %params ) = @_;

    my $dbi = database();
    my $id;
    $dbi->begin_work;
    eval {
        my $sth = $dbi->prepare(
            'INSERT INTO label(brewery, beer, year, notes) VALUES (?, ?, ?, ?)'
        ) or die $dbi->errstr;
        $sth->execute( $params{brewery}, $params{beer}, $params{year},
            $params{notes} );
        $id = $dbi->last_insert_id( '', '', '', '' )
          or die "Can't get last ID";
        $sth = $dbi->prepare(
'INSERT INTO labeltoimage(labelid, labelimageid) VALUES (?, ?)'
        ) or die $dbi->errstr;
        for my $imgid ( grep { $_ } split /,/, $params{labelids} ) {
            $sth->execute( $id, $imgid );
        }
    };
    if ($@) {
        $dbi->rollback;
        die $@;
    }
    else {
        $dbi->commit;
    }
    return $id;
}

sub update {
    my ($self, %params) = @_;

    my $dbi = database;
    $dbi->begin_work;
    eval {
        my $sth = $dbi->prepare('UPDATE label SET brewery=?, beer=?, year=?, notes=?, updated=current_timestamp WHERE id=?') or die $dbi->errstr;
        $sth->execute(@params{qw( brewery beer year notes id )});
        $sth = $dbi->prepare(
'INSERT INTO labeltoimage(labelid, labelimageid) VALUES (?, ?)'
        ) or die $dbi->errstr;
        for my $imgid ( grep { $_ } split /,/, $params{labelids} ) {
            $sth->execute( $params{id}, $imgid );
        }
        $sth = $dbi->prepare(
'UPDATE labeltoimage SET deleted=current_timestamp WHERE labelid=? AND labelimageid=?'
        ) or die $dbi->errstr;
        for my $imgid ( grep { $_ } split /,/, $params{deleted_labelids} ) {
            $sth->execute( $params{id}, $imgid );
        }
    };
    if ($@) {
        $dbi->rollback;
        die $@;
    }
    else {
        $dbi->commit;
    }
    return $params{id};
}

sub get {
    my ( $self, $id ) = @_;

    my $dbi         = database();
    my $data        = $dbi->quick_select( 'label', { id => $id } );
    my $fm          = LabelHistory::FileManager->new();
    my $labelimages = $fm->get_images_for_label($id);
    return undef unless $data;
    return LabelHistory::Label->new(
        { %$data, labelimages => $labelimages} );
}

true;
