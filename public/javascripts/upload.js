/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
  //  var url = window.location.hostname === 'blueimp.github.io' ?
  //              '//jquery-file-upload.appspot.com/' : 'server/php/';
    var url = '/upload';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var html = '<div class="editable-image" id="img_'+file.id+'">';
                html += '<img src="/label/'+file.url+'"/>';
                html += '<div class="remove-button-wrapper">';
                html += '<a class="remove-button btn" title="Remove this image" id="img_unsaved_delete_'+file.id+'"><span class="glyphicon glyphicon-remove"></span></a></button>';
                html += '</div></div>';
                var img = $(html);
                img.appendTo('#files');
                $('#img_unsaved_delete_'+file.id).click(function() {
                    removeimage_unsaved(file.id);
                });
                //$('<p/>').text(file.name).appendTo('#files');
                var imgids = $('#imgids');
                var currids = imgids.val();
                if (currids === undefined) {
                    currids = '';
                }
                imgids.val(currids + ',' + file.id);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function removeimage(id) {
    var imgids = $('#deleted_imgids');
    var currids = imgids.val();
    if (currids === undefined) {
        currids = '';
    }
    imgids.val(currids + ',' + id);
    $('#img_'+id).remove();
}

function removeimage_unsaved(id) {
    var imgids = $('#imgids');
    var currids = imgids.val();
    imgids.val(currids.replace(','+id,''));
    $('#img_'+id).remove();
    return false;
}
